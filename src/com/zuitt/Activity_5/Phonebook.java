package com.zuitt.Activity_5;
import java.util.ArrayList;
public class Phonebook {

  ArrayList<Contact> contacts;
  public Phonebook() {
    this.contacts = new ArrayList<>();
  }
  public Phonebook(ArrayList<Contact> contacts) {
    this.contacts = contacts;
  }
  public ArrayList<Contact> getContacts() {
    return contacts;
  }
  public void setContacts(Contact contact) {
    this.contacts.add(contact);
  }
  public void printPhonebook() {
    if(contacts.isEmpty()){
      System.out.println("Phone Book is empty");
      return;
    }
    for(Contact c : contacts){
      System.out.println(c.getName());
      System.out.println("----------------------------------------");
      System.out.println(c.getName() + " has the following registered numbers:");
      System.out.println(c.getContactNumber());
      System.out.println("----------------------------------------");
      System.out.println(c.getName() + " has the following registered addresses:");
      System.out.println(c.getAddress());
      System.out.println("========================================");
    }
  }
}

