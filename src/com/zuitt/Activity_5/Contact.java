package com.zuitt.Activity_5;

public class Contact {
  private String name;
  private String contactNumber;
  private String address;
  public Contact() {
  }
  public Contact(String name, String contactNumber, String address) {
    this.name = name;
    this.contactNumber = contactNumber;
    this.address = address;
  }
  public String getName() {
    return name;
  }
  public void setName(String name) {
    this.name = name;
  }
  public String getContactNumber() {
    return contactNumber;
  }
  public void addContactNumber(String newNumber) {
    if (this.contactNumber == null || this.contactNumber.isBlank()) {
      this.contactNumber = newNumber;
      return;
    }
    this.contactNumber += "\n" + newNumber;
  }
  public void setContactNumber(String contactNumber) {
    this.contactNumber = contactNumber;
  }
  public String getAddress() {
    return address;
  }
  public void setAddress(String address) {
    this.address = address;
  }
  public void addAddress(String address) {
    if (this.address == null || this.address.isBlank()) {
      this.address = address;
      return;
    }
    this.address += "\n" + address;
  }
}
