package com.zuitt.Activity_5;

public class Main {
  public static void main(String[] args) {
    Phonebook Phonebook = new Phonebook();
    Phonebook.printPhonebook();

    System.out.println();
    System.out.println("Adding Contacts...");
    System.out.println();

    Contact person1 = new Contact("John Doe", "+639152468596", "my home in Quezon city");
    person1.addContactNumber("+639228547963");
    person1.addAddress("my office in Makati City");

    Contact person2 = new Contact("Jane Doe", "+639165148573", "my home in Caloocan City");
    person2.addContactNumber("+639173698541");
    person2.addAddress("my office in Pasay City");

    Phonebook.setContacts(person1);
    Phonebook.setContacts(person2);
    Phonebook.printPhonebook();
  }
}

